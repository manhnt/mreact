import React from 'react';
import { Link } from 'react-router';

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {

    };
  }

  render() {
    return (
      <div>
        <p>Hello</p>
        <Link to="/test">Test route</Link>
        <div>
          {this.props.children}
        </div>
      </div>
    );
  }
}

App.propTypes = { children: React.PropTypes.object };

export default App;

