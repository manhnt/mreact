import React from 'react';
import { render } from 'react-dom';
import { Router, Route, hashHistory } from 'react-router';
import './style.scss';
import App from './components/App';
import Test from './components/Test';

window.React = React;

render(
  (<Router history={hashHistory}>
    <Route path="/" component={App}>
      <Route path="/test" component={Test} />
    </Route>
  </Router>), document.getElementById('app')
);

