#!/bin/bash
yarn clean-dist
babel-node tools/build.js
rm -rf public
mkdir public
cp dist/* public/
echo "Publish public folder to your webserver"

